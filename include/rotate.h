#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image.h"

struct image rotate_90( struct image const * const source );
struct image rotate_180( struct image const * const source );
struct image rotate_270( struct image const * const source );

#endif //IMAGE_TRANSFORMER_ROTATE_H
