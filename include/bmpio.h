#ifndef IMAGE_TRANSFORMER_BMPIO_H
#define IMAGE_TRANSFORMER_BMPIO_H

#include "bmp.h"

enum read_status read_bmp(char* filename, struct image* img);
enum write_status write_bmp(char* filename, struct image* img);

#endif //IMAGE_TRANSFORMER_BMPIO_H
