#ifndef ASSIGNMENT_5_SEPIA_FILTER_SEPIA_FILTER_H
#define ASSIGNMENT_5_SEPIA_FILTER_SEPIA_FILTER_H

#include "image.h"


struct image sepia_c(struct image img);
struct image sepia_asm(struct image img);

#endif //ASSIGNMENT_5_SEPIA_FILTER_SEPIA_FILTER_H
