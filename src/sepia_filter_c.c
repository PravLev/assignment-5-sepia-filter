#include "../include/sepia_filter_c.h"
#include <stdint.h>
#include <stddef.h>

static uint8_t sat_value(uint16_t x){
    return (x < 256 ? x : 255);
}

static inline struct pixel pixel_to_sepia(struct pixel pixel){
    struct pixel res = {
        .r =  sat_value((uint16_t)(0.393 * pixel.r + 0.769 * pixel.g + 0.189 * pixel.b)),
        .g =  sat_value((uint16_t)(0.349 * pixel.r + 0.686 * pixel.g + 0.168 * pixel.b)),
        .b =  sat_value((uint16_t)(0.272 * pixel.r + 0.534 * pixel.g + 0.131 * pixel.b))
    };
    return res;
}

struct image sepia_c(struct image img){
    struct image res = init(img.height, img.width);
    for(size_t i = 0; i < res.height; ++i){
        for(size_t j = 0; j < res.width; ++j){
            res.data[i * res.width + j] = pixel_to_sepia(img.data[i * res.width + j]);
        }
    }
    return res;

}

extern void sepia_nasm(struct pixel[4], struct pixel* res);

struct image sepia_asm(struct image img){
    struct image res = init(img.height, img.width);
    for(size_t i = 0; i < img.height * img.width; i += 4){
        sepia_nasm(img.data + i, res.data + i);
    }
    size_t end_p = res.width * res.height % 4;
    for(size_t i = 0; i < end_p; ++i){
        res.data[res.width * res.height - end_p + i] = pixel_to_sepia(img.data[res.width * res.height - end_p + i]);
    }
    return res;
}
