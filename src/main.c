#include "../include/bmpio.h"
#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/sepia_filter_c.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>


long get_current_time(void) {
    struct rusage r = {0};
    getrusage(RUSAGE_SELF, &r);
    return r.ru_utime.tv_usec;
}


int main(int argc, char **argv) {
    if (argc != 4 ) {
        fprintf(stderr,
                "Incorrect arguments. Enter the arguments in the following format: ./image-transformer <source-image> <transformed-image> <c|a>");
        return 0;
    }
    struct image img;
    switch (read_bmp(argv[1], &img)) {
        case READ_OK:
            break;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "Error when reading the file body");
            destructor_image(&img);
            return READ_INVALID_SIGNATURE;
        case READ_INVALID_HEADER:
            fprintf(stderr, "Error when reading the file header");
            destructor_image(&img);
            return READ_INVALID_HEADER;
        case READ_INVALID_FILE:
            fprintf(stderr, "Error opening the sours file");
            destructor_image(&img);
            return READ_INVALID_FILE;
        default:
            fprintf(stderr, "Error reading the file");
            destructor_image(&img);
            return READ_INVALID_FILE;
    }
    struct image sepia_img;
    long start = get_current_time();
    switch (argv[3][0]) {
        case 'c':
            sepia_img = sepia_c(img);
            break;
        case 'a':
            sepia_img = sepia_asm(img);
            break;
    }


    long end = get_current_time();
    printf("Execution time: %ld\n",end - start);
    switch (write_bmp(argv[2],&sepia_img)) {
        case WRITE_OK:
            destructor_image(&img);
            destructor_image(&sepia_img);
            break;
        case WRITE_ERROR:
            fprintf(stderr, "Error opening the converted file");
            destructor_image(&img);
            destructor_image(&sepia_img);
            return WRITE_ERROR;
    }

    return 0;
}
