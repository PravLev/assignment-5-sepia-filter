CFLAGS = -g -O0 -Wall -Werror -std=c17 -Wdiscarded-qualifiers -Wincompatible-pointer-types -Wint-conversion
BUILDDIR=build
SOURCEDIR=src

all: $(BUILDDIR)/bmp.o $(BUILDDIR)/bmpio.o $(BUILDDIR)/image.o $(BUILDDIR)/rotate.o $(BUILDDIR)/main.o $(BUILDDIR)/sepia_filter.o $(BUILDDIR)/sepia_filter_c.o
	gcc -no-pie -o $(BUILDDIR)/main $^

$(BUILDDIR)/%.o: $(SOURCEDIR)/%.c
	mkdir -p $(BUILDDIR)
	gcc -c $(CFLAGS) -o $@ $<

$(BUILDDIR)/%.o: $(SOURCEDIR)/%.asm
	mkdir -p $(BUILDDIR)
	nasm -felf64 -o $@ $<

clean:
	rm -rf $(BUILDDIR)


test:
	@echo "c:" ; \
	./$(BUILDDIR)/main input.bmp output_c.bmp c; \
	echo "nasm:" ; \
	./$(BUILDDIR)/main input.bmp output_a.bmp a; \


.phony: clean test